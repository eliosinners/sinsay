# sinsay

<figure>
  <img src="star.png" alt="">
</figure>

> Tell Yeoman what to say **the elioWay**

![release](https://elioway.gitlab.io/static/release.png "release")

This was a fun, early attempt at creating an npm module. It's completely unecessary!

This is what **elioWay** apps are all about - tinkering; seeing if you can do it; learning; having fun. If there was ever an **elioWay** module which encompasses what we're about, it's this one.

Yes. Don't reinvent the wheel unless you want to know how the wheel works.

<div><a href="installing.html">
  <button>Installing</button>
</a>
  <a href="quickstart.html">
  <button>Quickstart</button>
</a></div>

## See also

<dl>
  <dt>
  <a href="https://elioway.gitlab.io/eliosin/generator-sin">generator-sin</a>
</dt>
  <dd><strong>elioSin</strong>'s yeoman generator which using <strong>sinsay</strong> instead of <strong>yosay</strong> the <strong>elioWay</strong>.</dd>
</dl>
