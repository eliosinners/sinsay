# sinsay Credits

## Core! Thanks very much

- [yeoman:yosay](https://github.com/yeoman/yosay)

## Artwork

- [wikimedia:Clevelandart](https://commons.wikimedia.org/wiki/File:Clevelandart_1965.235.jpg)
- [ascii:skulls](http://ascii.co.uk/art/skulls)
- [ascii](http://patorjk.com/software/taag/#p=display&f=Big&t=elio)
