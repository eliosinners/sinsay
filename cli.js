#!/usr/bin/env node
"use strict"
const pkg = require("./package.json")
const sinsay = require(".")

require("taketalk")({
  init(input, options) {
    console.log(sinsay(input, options))
  },
  help() {
    console.log(`
  ${pkg.description}

  Usage
    $ sinsay <string>
    $ sinsay <string> --maxLength 8
    $ echo <string> | sinsay

  Example
    $ sinsay 'Sindre is a horse'
    ${sinsay("Sindre is a horse")}`)
  },
  version: pkg.version
})
